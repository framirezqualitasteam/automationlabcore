#Creado por: Edison Vasquez Burbano
#17 de Febrero 2021
#language: es

Característica: Validar acceso
  Como un usuario con permisos
  Quiero acceder a la aplicación de LabCore
  Para realizar el uso

  ######################################################################################
  ######################### ESCENARIO DE FLUJO CORRECTO ################################
  ######################################################################################


  Esquema del escenario: Validar acceso
    Dado que el usuario <nombre> abre un navegador en la pagina de LabCore
    Cuando el diligencia los datos de acceso
      | <usuario> | <password> | <compañia> | <sede> |
    Entonces el puede utilizar la aplicación de LabCore

    Ejemplos:
      | nombre           | usuario  | password      | compañia | sede       |
      | Fernando Ramirez | framirez | Qualitas2020* | LABCORE  | PONTEVEDRA |