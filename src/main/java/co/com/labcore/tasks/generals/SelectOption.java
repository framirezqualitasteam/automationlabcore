package co.com.labcore.tasks.generals;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.targets.Target;

import static co.com.labcore.ui.GeneralObjects.OPTION_SELECT;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class SelectOption implements Task {

    private final Target TARGET;
    private final String option;

    public SelectOption(Target target, String option) {
        this.TARGET = target;
        this.option = option;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(TARGET),
                Click.on(OPTION_SELECT.of(option))
        );
    }

    public static SelectOption from(Target target, String option) {
        return instrumented(SelectOption.class, target, option);
    }

}
