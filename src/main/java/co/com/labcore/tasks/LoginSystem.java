package co.com.labcore.tasks;

import co.com.labcore.models.UserModel;
import co.com.labcore.tasks.generals.SelectOption;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static co.com.labcore.ui.LabCoreLoginPage.*;
import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isEnabled;

public class LoginSystem implements Task {

    private final UserModel user;

    public LoginSystem(UserModel usuarioCliente) {
        this.user = usuarioCliente;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(user.getUser()).into(INPUT_USER),
                Enter.theValue(user.getPassword()).into(INPUT_PASSWORD),
                Click.on(BTN_LOGIN),
                SelectOption.from(SELECT_COMPANY, user.getCompany()),
                SelectOption.from(SELECT_CAMPUS, user.getCampus()),
                Click.on(BTN_LOGIN)
        );
    }

    public static LoginSystem withUser(UserModel user) {
        return instrumented(LoginSystem.class, user);
    }
}
