package co.com.labcore.ui;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class LabCoreLoginPage {

    public static final Target INPUT_USER = Target.the("input User").locatedBy("//mat-label[contains(text(),'Usuario')]/../../../input");
    public static final Target INPUT_PASSWORD = Target.the("input Password").locatedBy("//mat-label[contains(text(),'Contrase\u00f1a')]/../../../input");
    public static final Target SELECT_COMPANY = Target.the("select company").locatedBy("//mat-label[contains(text(),'Compañía')]/../../../..");
    public static final Target SELECT_CAMPUS = Target.the("select campus").locatedBy("//mat-label[contains(text(),'Sede')]/../../../..");

    public static final Target BTN_LOGIN = Target.the("login button").locatedBy("//button[@type='submit']");

}
