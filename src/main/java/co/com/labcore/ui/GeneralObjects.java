package co.com.labcore.ui;

import net.serenitybdd.screenplay.targets.Target;

public class GeneralObjects {

    public static final Target OPTION_SELECT = Target.the("select campus").locatedBy("//mat-option/span[contains(text(),'{0}')]");

}
