package co.com.labcore.ui;

import net.serenitybdd.screenplay.targets.Target;

public class LabCoreHomePage {

    public static final Target LBL_NAME_USER = Target.the("lbl name User").locatedBy("//strong[contains(text(),'{0} - {1}')]");

}
