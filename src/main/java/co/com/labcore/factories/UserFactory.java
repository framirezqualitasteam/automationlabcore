package co.com.labcore.factories;

import co.com.labcore.models.UserModel;

import java.util.List;

public class UserFactory {

    public static UserModel withData(List<String> datos) {
        UserModel user = new UserModel();
        user.setUser(datos.get(0));
        user.setPassword(datos.get(1));
        user.setCompany(datos.get(2));
        user.setCampus(datos.get(3));
        return user;
    }

}
