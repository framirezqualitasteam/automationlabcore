package co.com.labcore.models;

import lombok.Data;

@Data
public class UserModel {

    private String user;
    private String password;
    private String company;
    private String campus;

}
