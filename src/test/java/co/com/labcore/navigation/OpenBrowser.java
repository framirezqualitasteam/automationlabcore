package co.com.labcore.navigation;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

public class OpenBrowser implements Task {

    //private final WebDriver driver;
    public OpenBrowser() throws MalformedURLException {
        DesiredCapabilities caps = DesiredCapabilities.chrome();
        //driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), caps);
    }

    @Override
    @Step("{0} abre el navegador")
    public <T extends Actor> void performAs(T actor) {
        //actor.can(BrowseTheWeb.with(driver));
        actor.attemptsTo(Open.browserOn(new LabCore()));
        getDriver().manage().window().maximize();
        getDriver().manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    public static OpenBrowser inLabCore() {
        return instrumented(OpenBrowser.class);
    }
}
