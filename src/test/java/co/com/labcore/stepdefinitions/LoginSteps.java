package co.com.labcore.stepdefinitions;

import co.com.labcore.factories.UserFactory;
import co.com.labcore.models.UserModel;
import co.com.labcore.navigation.OpenBrowser;
import co.com.labcore.tasks.LoginSystem;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;

import java.util.List;

import static co.com.labcore.ui.LabCoreHomePage.LBL_NAME_USER;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isPresent;
import static net.serenitybdd.screenplay.questions.WebElementQuestion.the;

public class LoginSteps {

    private UserModel user;

    @Dado("^que el usuario (.*) abre un navegador en la pagina de LabCore$")
    public void userOpenBrowser(String nombre) {
        theActorCalled(nombre).wasAbleTo(OpenBrowser.inLabCore());
    }

    @Cuando("^el diligencia los datos de acceso$")
    public void fillCredentials(List<String> data) {
        user = UserFactory.withData(data);
        theActorInTheSpotlight().wasAbleTo(LoginSystem.withUser(user));
    }

    @Entonces("^el puede utilizar la aplicación de LabCore$")
    public void heUseApp() {
        theActorInTheSpotlight().should(seeThat(the(LBL_NAME_USER.of(user.getCompany(), user.getCampus())), isPresent()));
    }

}